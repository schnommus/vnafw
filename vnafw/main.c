#include <libopencm3/cm3/common.h>
#include <libopencm3/cm3/vector.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/efm32/wdog.h>
#include <libopencm3/efm32/gpio.h>
#include <libopencm3/efm32/cmu.h>
#include <libopencm3/efm32/timer.h>

#include <stdbool.h>
#include <stdio.h>

#include <toboot.h>

#include "cdcacm.h"

TOBOOT_CONFIGURATION(0);

/* Systick interrupt frequency, Hz */
#define SYSTICK_FREQUENCY 3000

/* USB (core clock) frequency of Tomu board */
#define USB_CLK_FREQUENCY 24000000

/* LED pin mappings */
#define LED_GREEN_PORT GPIOA
#define LED_GREEN_PIN  GPIO0
#define LED_RED_PORT   GPIOB
#define LED_RED_PIN    GPIO7

/* PIN diode bias mappings */
#define P1_BIAS_PORT   GPIOB
#define P1_BIAS_PIN    GPIO13
#define P2_BIAS_PORT   GPIOB
#define P2_BIAS_PIN    GPIO14

/* RF switch pin mappings */
#define TX_SWITCH_PORT GPIOB
#define TX_SWITCH_PIN  GPIO8
#define RX_SWITCH_PORT GPIOB
#define RX_SWITCH_PIN  GPIO11

// For consistent LED PWM brightness
const uint8_t gamma8[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
    10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
    17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
    25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
    37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
    51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
    69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
    90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
    115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
    144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
    177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
    215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255
};

volatile uint32_t system_333us = 0;

typedef enum {
    VNA_P1 = 0,
    VNA_P2,
    VNA_N_PORTS
} VNAPort;

typedef enum {
    VNA_S11 = 0,
    VNA_S12,
    VNA_S22,
    VNA_S21,
    VNA_N_PATHS
} VNAPath;

void vna_set_measurement_path(VNAPath path) {
    switch (path) {
        case VNA_S11:
            gpio_set(TX_SWITCH_PORT, TX_SWITCH_PIN);
            gpio_set(RX_SWITCH_PORT, RX_SWITCH_PIN);
            break;
        case VNA_S12:
            gpio_set(TX_SWITCH_PORT, TX_SWITCH_PIN);
            gpio_clear(RX_SWITCH_PORT, RX_SWITCH_PIN);
            break;
        case VNA_S22:
            gpio_clear(TX_SWITCH_PORT, TX_SWITCH_PIN);
            gpio_clear(RX_SWITCH_PORT, RX_SWITCH_PIN);
            break;
        case VNA_S21:
            gpio_clear(TX_SWITCH_PORT, TX_SWITCH_PIN);
            gpio_set(RX_SWITCH_PORT, RX_SWITCH_PIN);
            break;
        default:
            break; /* Do nothing for invalid paths */
    }
}

/* Reflect all signals at the designated port */
void vna_reflector_short(VNAPort port) {
    if (port == VNA_P1) {
        gpio_set(P1_BIAS_PORT, P1_BIAS_PIN);
    } else {
        gpio_set(P2_BIAS_PORT, P2_BIAS_PIN);
    }
}

/* Do not reflect signals at the designated port */
void vna_reflector_open(VNAPort port) {
    if (port == VNA_P1) {
        gpio_clear(P1_BIAS_PORT, P1_BIAS_PIN);
    } else {
        gpio_clear(P2_BIAS_PORT, P2_BIAS_PIN);
    }
}

void set_brightness(VNAPort p, uint8_t brightness) {
    if (p == VNA_P1)
        TIMER0_CC0_CCV = 0x100 - gamma8[brightness];
    else
        TIMER1_CC0_CCV = 0x100 - gamma8[brightness];
}

#define N_LED_STATES 4

uint8_t p1_led_steps[VNA_N_PATHS][N_LED_STATES] = {
    {0, 1, 1, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 0},
    {0, 1, 0, 0},
};

uint8_t p2_led_steps[VNA_N_PATHS][N_LED_STATES] = {
    {0, 0, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 1, 0},
    {0, 0, 1, 0},
};

/* Up to 1024 */
uint32_t led_sequence = 0;
void led_update(VNAPort p, uint8_t steps[]) {
    uint8_t lerp = led_sequence & 0x0FF;
    uint8_t step = (led_sequence & 0x300) >> 8;
    uint8_t target = steps[(step+1) % N_LED_STATES];
    uint8_t current = steps[step];
    if (target != current) {
        (target > current) ? set_brightness(p, lerp) :
                             set_brightness(p, 0xFF - lerp);
    }
}

void sys_tick_handler(void) {

    static VNAPath last_path = 0;
    VNAPath current_path = current_state();

    /* Reset LED state if the measurement path changed */
    if (current_path != last_path) {
        led_sequence = 0;
        set_brightness(VNA_P1, 0);
        set_brightness(VNA_P2, 0);
        last_path = current_path;
    }

    ++system_333us;

    /* Switch chops 66% measurement, 33% phase reference */
    bool measurement_path = (system_333us % 3 != 0);

    if (measurement_path) {
        /* Reflectors always open for measurement duty cycle */
        vna_reflector_open(VNA_P1);
        vna_reflector_open(VNA_P2);
        vna_set_measurement_path(current_path);
    } else {
        /* Engage appropriate path & reflector for the phase reference
         * duty cycle of this measurement. */
        switch (current_path) {
            case VNA_S11:
            case VNA_S12: {
                vna_set_measurement_path(VNA_S11);
                vna_reflector_short(VNA_P1);
            } break;
            case VNA_S22:
            case VNA_S21: {
                vna_set_measurement_path(VNA_S22);
                vna_reflector_short(VNA_P2);
            } break;
            default:
                break; /* Do nothing for invalid paths */
        }
    }

    if(system_333us % 0x3 == 0) {
        led_update(VNA_P1, p1_led_steps[current_path]);
        led_update(VNA_P2, p2_led_steps[current_path]);
        ++led_sequence;
    }
}

void setup_gpios(void) {
    /* GPIO peripheral clock is necessary for us to set up the GPIO pins as outputs */
    cmu_periph_clock_enable(CMU_GPIO);

    /* Set up all RF control ports as outputs */
    gpio_mode_setup(P1_BIAS_PORT, GPIO_MODE_PUSH_PULL, P1_BIAS_PIN);
    gpio_mode_setup(P2_BIAS_PORT, GPIO_MODE_PUSH_PULL, P2_BIAS_PIN);
    gpio_mode_setup(TX_SWITCH_PORT, GPIO_MODE_PUSH_PULL, TX_SWITCH_PIN);
    gpio_mode_setup(RX_SWITCH_PORT, GPIO_MODE_PUSH_PULL, RX_SWITCH_PIN);

    /* Unbias all the PIN reflectors */
    vna_reflector_open(VNA_P1);
    vna_reflector_open(VNA_P2);

    /* Default to S11 measurement path */
    vna_set_measurement_path(VNA_S11);

    /* Set up both LEDs as outputs */
    gpio_mode_setup(LED_RED_PORT, GPIO_MODE_WIRED_AND, LED_RED_PIN);
    gpio_mode_setup(LED_GREEN_PORT, GPIO_MODE_WIRED_AND, LED_GREEN_PIN);

    /* Configure timers for LED PWM */
    cmu_periph_clock_enable(CMU_TIMER0);
    cmu_periph_clock_enable(CMU_TIMER1);
    timer_set_clock_prescaler(TIMER0, TIMER_CTRL_PRESC_DIV64);
    timer_set_clock_prescaler(TIMER1, TIMER_CTRL_PRESC_DIV64);
    /* For a PWM frequency of (24MHz / 64) / 0xFF == ~1.5KHz */
    timer_set_top(TIMER0, 0xFF);
    timer_set_top(TIMER1, 0xFF);
    TIMER0_CC0_CTRL = TIMER_CC_CTRL_MODE_PWM;
    TIMER1_CC0_CTRL = TIMER_CC_CTRL_MODE_PWM;
    /* Connect TIMER0 CC output to the green LED */
    TIMER0_ROUTE = (TIMER_ROUTE_LOCATION_LOC0 << TIMER_ROUTE_LOCATION_SHIFT) |
                   TIMER_ROUTE_CC0PEN;
    /* Connect TIMER1 CC output to the red LED */
    TIMER1_ROUTE = (TIMER_ROUTE_LOCATION_LOC3 << TIMER_ROUTE_LOCATION_SHIFT) |
                   TIMER_ROUTE_CC0PEN;
    timer_start(TIMER0);
    timer_start(TIMER1);
    set_brightness(VNA_P1, 0);
    set_brightness(VNA_P2, 0);
}

int main(void)
{
    /* Disable the watchdog that the bootloader started. */
    WDOG_CTRL = 0;

    setup_gpios();

    /* Configure the system tick */
    /* Set the CPU Core to run from the trimmed USB clock, divided by 2.
     * This will give the CPU Core a frequency of 24 MHz +/- 1% */
    cmu_osc_on(USHFRCO);
    cmu_wait_for_osc_ready(USHFRCO);
    CMU_USBCRCTRL = CMU_USBCRCTRL_EN;
    CMU_CMD = CMU_CMD_HFCLKSEL(5);
    while (! (CMU_STATUS & CMU_STATUS_USHFRCODIV2SEL))
        ;

    systick_set_frequency(SYSTICK_FREQUENCY, USB_CLK_FREQUENCY);
    systick_counter_enable();
    systick_interrupt_enable();

    usb_cdcacm_init();

    /* Spin forever, SysTick interrupt will toggle the LEDs */
    while(1);
}
